<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $conn = @mysqli_connect("211.253.17.230:3306", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');
$conn = @mysqli_connect("localhost", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');

mysqli_set_charset($conn,"utf8");

// 오늘 날짜를 변수에 추가한다.
$date = date("Y-m-d");
// echo "현제시간: ".$date."<br>";

// 날짜 차이를 저장할 변수를 추가한다.
$base_time = 30;

// 오늘 날짜에서 특정 날짜 이전의 날짜로 변경
$date1 = date('Y-m-d', strtotime($date."-".$base_time."day"));
// echo "기준시간: ".$date1."<br>";

// echo "상품<br>";

// // 특정날짜 이전의 데이터를 가져온다(테스트용)
// $sql = "select index_no, gname, reg_time from shop_goods_history where date_format(reg_time,'%Y-%m-%d') <= '".$date1."' order by reg_time desc";
// $result = @mysqli_query($conn, $sql)or die('query_error01');
// while($row = $result->fetch_assoc()){
// 	print_r($row);
// 	echo "<br>";
// }

// echo "옵션<br>";

// $sql = "select io_no, io_id, reg_time from shop_goods_option_history where date_format(reg_time,'%Y-%m-%d') <= '".$date1."' order by reg_time desc";

// $result = @mysqli_query($conn, $sql)or die('query_error02');
// while($row = $result->fetch_assoc()){
// 	print_r($row);
// 	echo "<br>";
// }

// 특정날짜 이전의 값을 삭제한다
$sql = "delete from shop_goods_history where date_format(reg_time,'%Y-%m-%d') <= '".$date1."'";
$result = @mysqli_query($conn, $sql)or die('query_error03');

$sql = "delete from shop_goods_option_history where date_format(reg_time,'%Y-%m-%d') <= '".$date1."'";
$result = @mysqli_query($conn, $sql)or die('query_error04');
?>