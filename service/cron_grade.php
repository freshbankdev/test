<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $conn = @mysqli_connect("211.253.17.230:3306", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');
$conn = @mysqli_connect("localhost", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');

mysqli_set_charset($conn,"utf8");

// 등급 기간산출, 등급유지기간 설정값을 가져온다.
$sql = "SELECT * from shop_member_grade_option";
$result = @mysqli_query($conn, $sql)or die('query error01');
$row = $result->fetch_assoc();
// print_r($row);
// echo "<br><br>";

// 비교를 위해 오늘 날짜에서 유지기간을 뺀 날짜를 가져온다
$now_time = date("Y-m-d",strtotime("-".$row['gb_maintain']." month"));
// echo "현재 일시 : ".$now_time."<br/><br/>";

// 등급업 조건을 배열로 가져온다.
$sql = "select gb_no, gb_name , gb_buy_count, gb_buy_price  from shop_member_grade where gb_no > 1 order by gb_no desc";
$result = @mysqli_query($conn, $sql)or die('query error01');
while($row1 = $result->fetch_assoc()){
	$rows[] = $row1;
}

// print_r($rows);
// echo "<br><br>";
// 등급업
// 현재일에서 산출일을 뺀 날짜를 검색하기위해 -1을 곱해서 음수로 만든다.
$interval = $row['gb_interval'] * -1;
// 현재일에서 산출일 사이의 구매기록을 가져온다. 가져올때 관리자는 제외해야하므로 조건에 넣는다.
$sql = "SELECT b.index_no, b.name, b.grade, IFNULL(a.goods_price,0) as goods_price, 
IFNULL(a.cnt,0) as cnt, IFNULL(a.last_order,'0000-00-00 00:00:00') as last_order from
(select index_no,id,name,grade from shop_member where grade > 1) as b
left join
(SELECT index_no,mb_id,name,sum(goods_price) as goods_price, count(goods_price) as cnt, max(user_date) as last_order
from shop_order 
where user_date between date_add(now(),interval $interval month) and curdate() and dan = 5 and user_ok = 1 and mb_id != 'admin'
group by mb_id) as a
on a.mb_id = b.id order by index_no";
// echo $sql.'<br>';
$result = @mysqli_query($conn, $sql)or die('query error02');
while($rows1 = $result->fetch_assoc()){
	// print_r($rows1);
	// echo '<br>';

	// 등급업 쿼리를 초기화한다.
	$grade_up = "";
	// 회원의 등급과 등급 조건이 맟고 회원의 구입횟수가 등급조건의 구입횟수 이상이고 회원의 구매금액이 등급조건의 구매조건 이상일때.
	if($rows1["grade"] == $rows[0]['gb_no'] && $rows1["cnt"] >= $rows[0]['gb_buy_count'] && $rows1['goods_price'] >= $rows[0]['gb_buy_price'] && $rows[1]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[1]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[1]['gb_no'] && $rows1["cnt"] >= $rows[1]['gb_buy_count'] && $rows1['goods_price'] >= $rows[1]['gb_buy_price'] && $rows[2]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[2]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[2]['gb_no'] && $rows1["cnt"] >= $rows[2]['gb_buy_count'] && $rows1['goods_price'] >= $rows[2]['gb_buy_price'] && $rows[3]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[3]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[3]['gb_no'] && $rows1["cnt"] >= $rows[3]['gb_buy_count'] && $rows1['goods_price'] >= $rows[3]['gb_buy_price'] && $rows[4]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[4]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[4]['gb_no'] && $rows1["cnt"] >= $rows[4]['gb_buy_count'] && $rows1['goods_price'] >= $rows[4]['gb_buy_price'] && $rows[5]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[5]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[5]['gb_no'] && $rows1["cnt"] >= $rows[5]['gb_buy_count'] && $rows1['goods_price'] >= $rows[5]['gb_buy_price'] && $rows[6]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[6]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}else if($rows1["grade"] == $rows[6]['gb_no'] && $rows1["cnt"] >= $rows[6]['gb_buy_count'] && $rows1['goods_price'] >= $rows[6]['gb_buy_price'] && $rows[7]['gb_name'] != ""){
		$grade_up = "UPDATE shop_member set grade = ".$rows[7]['gb_no'].", last_grade_time = now() WHERE index_no = ".$rows1['index_no']." ";
	}
	if(!empty($grade_up)){
		$res_grade_up = @mysqli_query($conn, $grade_up)or die('query error05');
		// echo $grade_up;
		// echo "등급업이 완료되었습니다<br>";		
	}
}
// 강등
// 강등조건 기준을 가져온다
$interval1 = $row['gb_maintain'] * -1;
// 강등조건 기준으로 구매이력을 한번 더 검색한다.
$sql1 = "SELECT b.index_no, b.name, b.grade, ifnull(a.goods_price,0) as goods_price, 
ifnull(a.cnt,0) as cnt, b.last_grade_time from
(select index_no,name,grade,last_grade_time from shop_member where grade > 1) as b
left join
(SELECT index_no,name,sum(goods_price) as goods_price, count(goods_price) as cnt
from shop_order 
where user_date between date_add(now(),interval $interval1 month) and curdate() and dan = 5 and user_ok = 1 and mb_id != 'admin'
group by name) as a
on a.name = b.name order by index_no";
// echo $sql1;
$result1 = @mysqli_query($conn, $sql1)or die('query error03');
while($row2 = $result1->fetch_assoc()){ 
	$idx = $row2['index_no'];

	// 마지막 등급업 날짜
	$last_grade = date("Y-m-d",strtotime($row2['last_grade_time']));
	// 2019년 9월 16일 기준으로 아직 3개월 이전에 구입한 사람이 없으므로 테스트를 위한 날짜변수를 추가함.
	$date11 = date("Y-m-d",strtotime($row2['last_grade_time']."-4 months"));

	// print_r($row2);
	// echo "<br>";

	// sql정보 초기화
	$grade_down = "";
	// 회원의 등급과 등급 조건이 맟고 회원의 구입횟수가 등급조건의 구입횟수 이상이고 회원의 구매금액이 등급조건의 구매조건 이상이고, 마지막 등급업 기준일이 등급조건의 유지기간 이하일때 강등된다.
	if($row2['grade'] == $rows[1]['gb_no'] && $row2["cnt"] < $rows[1]['gb_buy_count'] && $row2['goods_price'] < $rows[1]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[0]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}else if($row2['grade'] == $rows[2]['gb_no'] && $row2["cnt"] < $rows[2]['gb_buy_count'] && $row2['goods_price'] < $rows[2]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[1]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}else if($row2['grade'] == $rows[3]['gb_no'] && $row2["cnt"] < $rows[3]['gb_buy_count'] && $row2['goods_price'] < $rows[3]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[2]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}else if($row2['grade'] == $rows[4]['gb_no'] && $row2["cnt"] < $rows[4]['gb_buy_count'] && $row2['goods_price'] < $rows[4]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[3]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}else if($row2['grade'] == $rows[5]['gb_no'] && $row2["cnt"] < $rows[5]['gb_buy_count'] && $row2['goods_price'] < $rows[5]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[4]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}else if($row2['grade'] == $rows[6]['gb_no'] && $row2["cnt"] < $rows[6]['gb_buy_count'] && $row2['goods_price'] < $rows[6]['gb_buy_price'] && $now_time > $last_grade){
		$grade_down = "UPDATE shop_member set grade = ".$rows[5]['gb_no'].", last_grade_time = now() WHERE index_no = $idx ";
	}

	if(!empty($grade_down)){
		$result2 = @mysqli_query($conn, $grade_down)or die('query error04');
		// echo $grade_down;
		// echo "강등이 완료되었습니다<br>";
	}
}
?>