<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $conn = @mysqli_connect("211.253.17.230:3306", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');
$conn = @mysqli_connect("localhost", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');

mysqli_set_charset($conn,"utf8");

// 오늘 날짜를 가지고 오는 date 변수
$date1 = date("Y-m-d");
$date = date("Y-m-d", strtotime($date1." 0 day"));
// echo $date.'<br>';
echo "테스트 시간 : ". date("Y-m-d H:i")."<br/>";

$temp_array = array();

// 1. 전날 시세를 몽땅 끌고온다. 새로 생긴 상품이 있을 수 있기때문에 카테고리 번호값도 전부 끌고온다.
$sql = "SELECT *, a.catename as catename  from 
(select catecode,catename from shop_category where char_length(catecode)=9) as a
left join
(select * from siseList where reg_time = date_format(date_add(now(),interval -1 day), '%Y-%m-%d')) as b
on a.catecode = b.ca_id";
$result = @mysqli_query($conn, $sql)or die('query error01');
while($row = $result->fetch_assoc()){
  // 초기화

  // print_r($row);
  // echo '<br>';
  $ca_id = $row['catecode'];

  // 가격 비교를 위한 최소값과 최대값을 가져온다
  $max = $row['goods_price'] * 1.1;
  $min = $row['goods_price'] * 0.9;

  $max1 = $row['goods_price1'] * 1.1;
  $min1 = $row['goods_price1'] * 0.9;

  $temp_origin = $row['origin'];
  $temp_brand_nm = $row['brand_nm'];
  $temp_grade = $row['grade'];

  // 카테고리 번호를 쿼리문 조건에 넣는다.
  $where = "and ca_id = '".$ca_id."' ";
  // echo $ca_id."<br>"; 

  // 오늘 상품의 가격을 가져온다.
  $sql1 = "SELECT index_no, ca_id, gname,
  IFNULL(truncate((goods_price),0),0) as goods_price, 
  IFNULL(truncate(avg(goods_price1),0),0) as goods_price1,
  origin,
  brand_nm,
  grade
  from shop_goods 
  where shop_state= 0 and isopen = '1' and char_length(ca_id)=9 and meat in ('소고기', '돼지고기', '고기')
  $where
  group by ca_id, origin, brand_nm, grade";
  $result1 = @mysqli_query($conn, $sql1)or die('query error01');
  while($row1 = $result1->fetch_assoc()){
    $temp_index_no = $row1['index_no'];
    if(in_array($temp_index_no,$temp_array)){
      continue;
    }else{
      array_push($temp_array, $temp_index_no);  
    }

    // 2-1. 전날 시세가 없으면 오늘 등록된 상품들을 시세로 등록한다.
    if($row['goods_price'] == 0 && $row['goods_price1'] == 0 || empty($row1['goods_price']) && empty($row1['goods_price1'])){
      $goods_price = $row1['goods_price'];
      $goods_price1 = $row1['goods_price1'];
      $m_good_price = 0;
      $m_good_price1 = 0;
      $p_goods_price = 0;
      $p_goods_price1 = 0;

      $origin = $row1['origin'];
      $brand_nm = $row1['brand_nm'];
      $grade = $row1['grade'];
    }else{
      // 2-2. 전날 시세가 있으면 오늘 등록된 상품들을 루프 돌리면서 전날시세와 3가지내용(카테고리 ID , 원산지, 브랜드명)이 같을시 시세조건을 한번더 검사후 
      // 조건에 맞으면 평균가격을 오늘 시세로 등록한다. 조건에 맞지 않으면 전날시세를 그대로 등록한다.
      $origin = $row1['origin'];
      $brand_nm = $row1['brand_nm'];
      $grade = $row1['grade'];

      // 원산지, 브랜드, 등급이 전날 있었던 경우
      if($row['origin']==$row1['origin'] && $row['brand_nm']==$row1['brand_nm'] && $row['grade']==$row1['grade']){
        // 시세가격보다 +-10퍼센트 가격 이내일 경우 오늘의 가격과 비교하여 시세차를 구한다.
        if($row1['goods_price'] >= $min && $row1['goods_price'] <= $max && $row1['goods_price1'] >= $min1 && $row1['goods_price1'] <= $max1){
          $goods_price = $row1['goods_price'];
          $goods_price1 = $row1['goods_price1'];

          $m_good_price = $row1['goods_price'] - $row['goods_price'];
          $m_good_price1 = $row1['goods_price1'] - $row['goods_price1'];
          if($m_good_price1!=0 || $m_good_price!=0){
            $p_goods_price = $m_good_price / $row['goods_price'] * 100;
            $p_goods_price1 = $m_good_price1 / $row['goods_price1'] * 100;  
          }
          else{
            $p_goods_price = 0;
            $p_goods_price1 = 0;
          }
        }else{
          // 시세가가 전날대비 10퍼센트 이상 차이날경우 전날시세을 등록한다.
          $goods_price = $row['goods_price'];
          $goods_price1 = $row['goods_price1'];

          $m_good_price = 0;
          $m_good_price1 = 0;
          $p_goods_price = 0;
          $p_goods_price1 = 0;
        }
      }
      else {
        // 새로운 원산지, 브랜드, 등급이 추가된경우 시세차가 없으므로 시세차를 0으로 고정한다.
        $goods_price = $row1['goods_price'];
        $goods_price1 = $row1['goods_price1'];

        $m_good_price = 0;
        $m_good_price1 = 0;
        $p_goods_price = 0;
        $p_goods_price1 = 0;
      }
    }        
    // print_r($row1);
    // echo '<br>';
    // echo "insert into siseList(ca_id,catename,goods_price,goods_price1,reg_time,mp_goods_price,mp_goods_price1,mp_percent,mp_percent1,origin,brand_nm,grade) values('".$ca_id."','".$row['catename']."',".$goods_price.",".$goods_price1.",'".$date."','".$m_good_price."','".$m_good_price1."','".round($p_goods_price,2)."','".round($p_goods_price1,2)."','".$origin."','".$brand_nm."','".$grade."')<br>";
    // 결과값을 업데이트 한다.
    $sql2 = "insert into siseList(ca_id,catename,goods_price,goods_price1,reg_time,mp_goods_price,mp_goods_price1,mp_percent,mp_percent1,origin,brand_nm,grade) values('".$ca_id."','".$row['catename']."',".$goods_price.",".$goods_price1.",'".$date."','".$m_good_price."','".$m_good_price1."','".round($p_goods_price,2)."','".round($p_goods_price1,2)."','".$origin."','".$brand_nm."','".$grade."')";
    $result2 = @mysqli_query($conn, $sql2)or die('query error02');
  }; 
}
?>