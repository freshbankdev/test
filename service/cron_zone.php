<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// $conn = @mysqli_connect("211.253.17.230:3306", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');
$conn = @mysqli_connect("localhost", "freshbank", "alxmrmfls@#2019", "freshbank") or die('sql connect fail');

mysqli_set_charset($conn,"utf8");

// 오늘 날짜를 가지고 오는 date 변수
$date1 = date("Y-m-d");
$date = date("Y-m-d", strtotime($date1." 0 day"));
// echo $date.'<br>';
echo "테스트 시간 : ". date("Y-m-d H:i")."<br/>";

$good_array = array();
$i = 0;
$zone = "";
// 1. 판매중인 모든 상품의 정보를 가지고 온다
$sql = "SELECT index_no, zone FROM shop_goods ORDER BY index_no asc";
$result = @mysqli_query($conn, $sql)or die('query error01');
while($row = $result->fetch_assoc()){
	// print_r($row);
	// echo "<br>";
	$zone = "";
	$sql_zone1 = "SELECT GROUP_CONCAT(DISTINCT level1_text order by level1_text) as zone1 from geo_list where find_in_set (level3_idx,
	(select zone from shop_goods where index_no = '{$row['index_no']}'))";
	$result1 = @mysqli_query($conn, $sql_zone1)or die('query error02');	
	while($row1 = $result1->fetch_assoc()){
		if(isset($row1['zone1'])){
			$zone = $row1['zone1'];
		}
	}

	$sql_zone2 = "SELECT GROUP_CONCAT(DISTINCT(level1_text)) as zone2 from geo_list where find_in_set (level1_code,
	(select zone from shop_goods where index_no = '{$row['index_no']}'))";
	$result2 = @mysqli_query($conn, $sql_zone2)or die('query error02');	
	while($row2 = $result2->fetch_assoc()){
		if(isset($row2['zone2'])){
			$zone = $row2['zone2'];
		}
	}
	// echo $row['index_no']."::";
	// print_r($zone);
	// echo "<br>";
	// echo "UPDATE shop_goods set zone_text='{$zone}' where index_no = '{$row['index_no']}'"."<br>";
	$sql_up = "UPDATE shop_goods set zone_text='{$zone}' where index_no = '{$row['index_no']}'";
	$result3 = @mysqli_query($conn, $sql_up)or die('query error03');	
}

// $sql = "select index_no,
// CONCAT(
// if(zone like '%9999999999%','전국',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%서울%') IN BOOLEAN MODE)) != 0,'서울,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%부산%') IN BOOLEAN MODE)) != 0,'부산,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%대구%') IN BOOLEAN MODE)) != 0,'대구,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%인천%') IN BOOLEAN MODE)) != 0,'인천,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%광주%') IN BOOLEAN MODE)) != 0,'광주,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%대전%') IN BOOLEAN MODE)) != 0,'대전,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%울산%') IN BOOLEAN MODE)) != 0,'울산,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%세종%') IN BOOLEAN MODE)) != 0,'세종,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%경기%') IN BOOLEAN MODE)) != 0,'경기,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%강원%') IN BOOLEAN MODE)) != 0,'강원,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%충청북도%') IN BOOLEAN MODE)) != 0,'충청북도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%충청남도%') IN BOOLEAN MODE)) != 0,'충청남도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%전라북도%') IN BOOLEAN MODE)) != 0,'전라북도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%전라남도%') IN BOOLEAN MODE)) != 0,'전라남도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%경상북도%') IN BOOLEAN MODE)) != 0,'경상북도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%경상남도%') IN BOOLEAN MODE)) != 0,'경상남도,',''),
// if((match(zone) against((select CONCAT(GROUP_CONCAT(level3_idx separator ' ')) from geo_list where level1_text like '%제주%') IN BOOLEAN MODE)) != 0,'제주','')) as able_zone
// from shop_goods;";
// $result = @mysqli_query($conn, $sql)or die('query error01');
// while($row = $result->fetch_assoc()){
// 	print_r($row);
// 	echo "<br>";
// }
?>
